import 'package:api_integrate/ui/search/drinks_details_screen.dart';
import 'package:flutter/material.dart';

import '../models/api_responses/drinks_data.dart';

class AppRouter{
  void goToDrinksDetailsScreen(Drinks drink, BuildContext context) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => DrinksDetailsScreen(drink)));
  }
}