import 'package:api_integrate/providers/search_provider.dart';
import 'package:api_integrate/ui/search/search_drinks_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<SearchProvider>(create: (_) => SearchProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.amber,
          backgroundColor: Colors.white,
          primaryColor: Colors.purple,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          hintColor: const Color(0x661E0C52),
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          textTheme: const TextTheme(
            headline5: TextStyle(fontSize: 18, fontWeight: FontWeight.w400),
            headline6: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
            bodyText1: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
            caption: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
            overline: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.w400,
                letterSpacing: 0.6,
                wordSpacing: 0.6),
          ),
        ),
        home: const SearchPage(),
      ),
    );
  }
}
