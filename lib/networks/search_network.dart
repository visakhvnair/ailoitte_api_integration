import 'package:http/http.dart' as http;

import '../constants/network_constants.dart';


class SearchNetwork {
  Future<http.Response> fetchSearchedDrinks(String searchKey) async {
    Uri uri = Uri.parse(NetworkConstants.baseUrl +
        '/api/json/v1/1/search.php?s=$searchKey');

    return await http.get(uri);
  }
}
