import 'dart:convert';

import 'package:api_integrate/models/api_responses/drinks_data.dart';
import 'package:api_integrate/networks/search_network.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class SearchProvider extends ChangeNotifier {
  bool isSearching = false;
  bool isNoResult = false;
  List<Drinks> drinksList = [];

  void searchDrinks(String searchKey) {
    if (isSearching) return;

    if (searchKey.isEmpty) return;

    _searchDrinks(searchKey);
  }

  void _searchDrinks(String searchKey) async {
    isSearching = true;
    drinksList = [];
    isNoResult = false;
    notifyListeners();
    try {
      Response response = await SearchNetwork().fetchSearchedDrinks(searchKey);
      if (response.statusCode == 200) {
        drinksList = DrinksData.fromJson(jsonDecode(response.body)).drinks;
        isNoResult = drinksList.isEmpty;
      } else {
        showErrorMessage("Something went wrong, Please try again");
      }
      isSearching = false;
    } catch (e) {
      isSearching = false;
      showErrorMessage("Something went wrong, Please try again");
    }
    notifyListeners();
  }

  void showErrorMessage(String message) {
    // ToDo
    // Develop a method to show error message on screen.
  }
}
