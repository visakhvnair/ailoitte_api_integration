import 'package:flutter/material.dart';

import '../../models/api_responses/drinks_data.dart';

class DrinksDetailsScreen extends StatelessWidget {
  final Drinks drink;
  const DrinksDetailsScreen(this.drink, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: ListView(
          children: [
            const SizedBox(
              height: 16,
            ),
            AspectRatio(
              aspectRatio: 1,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: drink.strDrinkThumb != null
                    ? Image.network(drink.strDrinkThumb ?? "")
                    : Text(
                        "No Image Available",
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            ?.copyWith(color: Colors.black),
                      ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              drink.strDrink ?? "Drink Name Not Available",
              style: Theme.of(context)
                  .textTheme
                  .headline5
                  ?.copyWith(color: Colors.black),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              drink.strInstructions ?? "No Instruction Available",
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  ?.copyWith(color: Colors.black87),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              "Glass : ${drink.strDrink ?? "Not Available"}",
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  ?.copyWith(color: Colors.black54),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              "Tags : ${drink.strTags ?? "No Tags"}",
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  ?.copyWith(color: Colors.black38),
            ),
          ],
        ),
      )),
    );
  }
}
