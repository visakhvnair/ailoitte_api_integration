import 'package:api_integrate/app_router/app_router.dart';
import 'package:api_integrate/models/api_responses/drinks_data.dart';
import 'package:api_integrate/providers/search_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../widgets/custom_progress_widget.dart';

class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(80),
        child: Container(
            margin: const EdgeInsets.only(top: 24), child: SearchBarWidget()),
      ),
      body: SafeArea(
        child: Consumer<SearchProvider>(
          builder: (context, searchProvider, child) {
            if (searchProvider.drinksList.isEmpty &&
                searchProvider.isSearching) {
              return Container();
            }
            if (searchProvider.isNoResult) {
              return Center(
                child: Text(
                  "No Result",
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      ?.copyWith(color: Colors.black),
                ),
              );
            }
            if (searchProvider.drinksList.isNotEmpty) {
              return ListView.separated(
                padding: const EdgeInsets.only(top: 16, bottom: 24),
                separatorBuilder: (context, index) => const Divider(
                  height: 12,
                  color: Colors.transparent,
                ),
                itemCount: searchProvider.drinksList.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      AppRouter().goToDrinksDetailsScreen(searchProvider.drinksList[index], context);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child:
                          DrinksListItemView(searchProvider.drinksList[index]),
                    ),
                  );
                },
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}

class DrinksListItemView extends StatelessWidget {
  final Drinks drink;

  const DrinksListItemView(this.drink, {Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            drink.strDrink ?? "No Name Available",
            style: Theme.of(context).textTheme.bodyText1,
          ),
          const SizedBox(
            height: 4,
          ),
          Text(
            drink.strInstructions ?? "No Instruction Available",
            style: Theme.of(context)
                .textTheme
                .bodyText1
                ?.copyWith(color: Colors.black38),
          ),
        ],
      ),
    );
  }
}

class SearchBarWidget extends StatelessWidget {
  final TextEditingController _searchEditCntlr = TextEditingController();
  SearchBarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
      child: Row(
        children: [
          Expanded(
              flex: 4,
              child: TextField(
                // autofocus: true,
                autocorrect: true,
                controller: _searchEditCntlr,
                textInputAction: TextInputAction.search,
                onSubmitted: (value) {
                  Provider.of<SearchProvider>(context, listen: false)
                      .searchDrinks(_searchEditCntlr.text);
                },
                decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: const EdgeInsets.only(left: 8),
                  hintText: 'Search',
                  hintStyle: Theme.of(context).textTheme.bodyText1,
                ),
                style: Theme.of(context).textTheme.bodyText1,
                keyboardType: TextInputType.text,
              )),
          if (context.watch<SearchProvider>().isSearching)
            const Center(
              child: SizedBox(
                height: 30,
                width: 30,
                child: CustomProgressWidget(),
              ),
            )
          else
            InkWell(
              onTap: () {
                Provider.of<SearchProvider>(context, listen: false)
                    .searchDrinks(_searchEditCntlr.text);
              },
              child: const Icon(
                Icons.search,
                color: Colors.black,
              ),
            )
        ],
      ),
    );
  }
}
