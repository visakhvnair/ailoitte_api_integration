import 'package:flutter/material.dart';

class CustomProgressWidget extends StatelessWidget {
  const CustomProgressWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const CircularProgressIndicator(
      backgroundColor: Colors.transparent,
      color: Colors.red,
      strokeWidth: 2,
    );
  }
}
